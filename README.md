# HomeMaster-Driver
HomeMaster网关驱动部分

## 智能家居DIY项目 v2018版
----
该工程属于属于智能家居DIY项目的一部分，该项目主要包括:
- [**Home** iOS应用程序，由Swift编写，负责家居设备、场景、自动化的查看与设置](https://gitee.com/adaidesigner/home)
- [**HomeMaster** 家电控制网关(c+golang)，负责设备传感器的控制与查询](https://gitee.com/adaidesigner/homemaster)
- [**HomeServer** 服务器应用程序(golang)，负责设备、手机端接入，以及家居场景自动化功能](https://gitee.com/adaidesigner/homeserver)
- **HomeMaster-Driver** 硬件设备驱动，由C语言编写的内核模块

## 驱动说明
[HomeMaster PCB原理图](homemaster.pdf)
![](hardware.png)

## PCB制板3D外观
所有元器件都有3D模型，方便导入CreoParameteric进行装配
Altium Designer PCB3D模型截图
![](altiumdesigner.png)
