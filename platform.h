//
// Created by Yun Zeng on 2018/4/14.
//

#ifndef PLATFORM_H
#define PLATFORM_H

#include <linux/types.h>
#include "master.h"

typedef enum {
    MasterHTSensor = 0x01,
    MasterLightSensor = 0x02,
    MasterOled = 0x11,
    MasterRadio = 0x21,
    MasterInfrared = 0x22,
}platform_module_type;

int platform_init(master_dev_typedef *dev);
int platform_exit(master_dev_typedef *dev);

int32_t platform_read(uint8_t *data, uint16_t len);
int32_t platform_write(uint8_t *data, uint16_t len);

int platform_open(void);
int platform_close(void);

#endif // PLATFORM_H
