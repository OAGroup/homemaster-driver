//
// Created by Yun Zeng on 2018/4/14.
// 温湿度计 SHT20
//

#ifndef PLATFORM_HT_H
#define PLATFORM_HT_H

#include <linux/types.h>

#pragma pack(push)
#pragma pack(1)

typedef struct {
    uint16_t temperature;
    uint16_t humidity;
}platform_ht_data_typedef;

#pragma pack(pop)

int32_t platform_ht_write(uint8_t *data, uint16_t len);

typedef int (*ht_recv_callback)(platform_ht_data_typedef *data);
int platform_ht_init(ht_recv_callback callback);
int platform_ht_exit(void);


#endif // PLATFORM_HT_H
